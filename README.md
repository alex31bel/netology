[![Netology-Logo](./img/netology-logo.png)](https://netology.ru/sharing/59a7c1820949ec7f6540e05fdd62b8bc)
# Курс - Системный администратор (июль 2022 - август 2023). Группа - SYS-17.
Выполненная домашняя работы за время обучения.

## IT-системы и Linux
*Обзор IT-систем*
- [Принципы работы современных компьютеров: процессоры, память, накопители. GDocs](https://docs.google.com/document/d/1guzCbqSS-uiJCC717RjP6-ZHwCBHtxqZmDBA1eJl-iU) - [PDF](./pdf/1.pdf)
- [Средства автоматизации и основные функции систем. GDocs](https://docs.google.com/document/d/1ks0tkUF-FSv1oGFZRGWBIJkknWYBcaqxBqxkfk_Pfug) - [PDF](./pdf/2.pdf)

*Операционная система Linux*
- [Типы и назначение операционных систем. ОС Linux. GDocs](https://docs.google.com/document/d/1mPe-4GdVzxoQM0sYjdEqk0RdNaQalpnWhhJ4F2_a56A) - [PDF](./pdf/3.pdf)
- [Основы работы с командной строкой. GDocs](https://docs.google.com/document/d/1bMpleRr5_XYXmZieDA-Mx-_5LDNr9pwyO6qQ-E9Km48) - [PDF](./pdf/4.pdf)
- [Процессы, управление процессами. GDocs](https://docs.google.com/document/d/1fX7DdkjGGXkhkG3-LHoBZhEPLe6QSBMSYo5dSP2J2SI) - [PDF](./pdf/5.pdf)
- [Память, управление памятью. GDocs](https://docs.google.com/document/d/1Tv6GTOTkk5-dkTC3XOr8x_-ISrCqnUKGgG0Xnnd3rH0) - [PDF](./pdf/6.pdf)
- [Шедулер](https://docs.google.com/document/d/1VUpYcKyhiuu7WNif-LRgXQBXuLrVkmrbDSPbNwPuriA) - [PDF](./pdf/7.pdf)
- [Дисковые системы. GDocs](https://docs.google.com/document/d/1Pd8ZKCPTx5IdDDhmb1p276b1hoR5W9e-KTJAFbtpj20) - [PDF](./pdf/8.pdf)
- [Файловые системы. GDocs](https://docs.google.com/document/d/1-U5pr_ZFz40J_uHZdEna864vFen7ohgTY0dOq97S6h0) - [PDF](./pdf/9.pdf)
- [Ядро операционной системы. GDocs](https://docs.google.com/document/d/118RHnFxTlP-a2eOJts7v44INlkRvVc-UxZ2SThB3E74) - [PDF](./pdf/10.pdf)
- [Загрузка ОС. GDocs](https://docs.google.com/document/d/1c4V6uxZDnPbJOiPqFO_SXruQ6a4YKixwjXF8ceOnNkM) - [PDF](./pdf/11.pdf)

*Администрирование Linux*
- [Типы дистрибутивов. GDocs](https://docs.google.com/document/d/1oNgaRyPFDxqdYjT8aZLpKuXD90XbQbjtgb2rRwi7MNw) - [PDF](./pdf/12.pdf)
- [Управление пакетами. GDocs](https://docs.google.com/document/d/1TtKTf8_c7QAJdHua0Gr21QjX8d03utCcBUWr0bR_YOg) - [PDF](./pdf/13.pdf)
- [Инициализация системы. Systemd, init-v. GDocs](https://docs.google.com/document/d/1ucibSIggQdXXWzfvJqE3VTUeh-Iypxu40aagdYLCZ40) - [PDF](./pdf/14.pdf)
- [Управление пользователями. GDocs](https://docs.google.com/document/d/13V3OVk5M-NhsNzCfcyVDOcxA6ZvuGwPAh0cHXqImzD0) - [PDF](./pdf/15.pdf)
- [Производительность системы. GDocs](https://docs.google.com/document/d/1_2UV5hck04lCM4Ngbn55h4h6_PbkN8M1IoEEUF2xQuQ) - [PDF](./pdf/16.pdf)
- [Производительность системы. Часть 2. GDocs](https://docs.google.com/document/d/1sh7h7RsGU2sjjZFO_YikgpAWwjxDEFx7ayumU612MI0) - [PDF](./pdf/17.pdf)

## Сеть, сетевые протоколы и Bash
*Сеть и сетевые протоколы*
- [Модель OSI/ISO. Обзор сетевых протоколов. GDocs](https://docs.google.com/document/d/19uvs99rV867eTi9XVqrR4Z6S4ETOV180i_KWHWxnqvw) - [PDF](./pdf/18.pdf)
- [L2-сеть. GDocs](https://docs.google.com/document/d/1UD2MEJFInROT_Nt5jneAjF0HqbR4bKSgg9fZE8HTB40) - [PDF](./pdf/19.pdf)
- [L3-сеть. GDocs](https://docs.google.com/document/d/1muJ_7_bY9nLlzEhixmoEQwwDYlhmCKJ3JWPcQzmZOQA) - [PDF](./pdf/20.pdf)
- [L4-сеть. GDocs](https://docs.google.com/document/d/1LZOAQ8XUx3EltLPvksf3r9Izkclac6WqEhxC4sA4lj0) - [PDF](./pdf/21.pdf)
- [Firewall. GDocs](https://docs.google.com/document/d/1scwXkZ3si5xjC5Eb2eV_InrwNqjy6l96WS0XrC54s5M) - [PDF](./pdf/23.pdf)
- [NAT. GDocs](https://docs.google.com/document/d/1DzQwG9j2BmP1H1T02RMkNKJvvK-ZKB0wAPMdgdV4VXM) - [PDF](./pdf/24.pdf)
- [VPN. GDocs](https://docs.google.com/document/d/19kaSmY5CX_V_YAlLLWUZu9cp4Ii9iSX7_KAlmxhg5Hg)
- [Высокоуровневые сетевые протоколы. GDocs](https://docs.google.com/document/d/1Ra3P7DDClIvM_3XzcpHOWXCyjVXMgB_q8nfuO-8uBp4) - [PDF](./pdf/25.pdf)
- [DHCP, PXE. GDocs](https://docs.google.com/document/d/1XGIvgxJAMEbw3idXvh7T3NNIwZprQLIvEs97-q2TnMk) - [PDF](./pdf/27.pdf)
- [DNS. GDocs](https://docs.google.com/document/d/1JXBo5VY8-VI27AmdgHPFy-LV4or0YVM60U9WA54NmkA) - [PDF](./pdf/28.pdf)
- [HTTP/HTTPS. GDocs](https://docs.google.com/document/d/1ln7JCM-J_js9jRXymBikUxvbU-yZKGl44W3427hLX0E) - [PDF](./pdf/29.pdf)
- [IPv6. GDocs](https://docs.google.com/document/d/1U5-8WySFpbe4CUjQr1kl2VXlTR9_vQey--5YjtdfDYo) - [PDF](./pdf/30.pdf)

*Программирование на Bash*
- [Программирование на Bash. Переменные и условные операторы. GDocs](https://docs.google.com/document/d/1_JpWwE1Hq09BUYwQg06yd4hjY4abvGVDASP7jzGrGGM) - [PDF](./pdf/31.pdf)
- [Программирование на Bash. Циклы и функции. GDocs](https://docs.google.com/document/d/1CBkJcCMb4hzvfzLg6Kup8BuJeQtW8RfO4qIaYIFdQGM) - [PDF](./pdf/32.pdf)

## Виртуализация, автоматизация и CI/CD
*Виртуализация*
- [Виртуализация и облачные решения. AWS, GCP, Яндекс.Облако, Openstack. GDocs](https://docs.google.com/document/d/1BlMbDJpZjQxP0tOQWIRBYNvX2ixPjwj2XqE1VppgCjk) - [PDF](./pdf/36.pdf)
- [Типы виртуализаций KVM, QEMU. GDocs](https://docs.google.com/document/d/1_d5UdolI_xTNLD2xV2kQ-2MGnxedkBoDyCjTYWSoNwA) - [PDF](./pdf/37.pdf)
- [Docker. GDocs](https://docs.google.com/document/d/1v_SAZj-OrTmdx9z3g5JvysDLWKOg3gzPdgOWKPRj610) - [PDF](./pdf/38.pdf)
- [Docker. Часть 2. GDocs](https://docs.google.com/document/d/1DrkOhPq_TtZJ71D3VKLPipCIkOjqQ5-G-q6LqRgFIBY) - [PDF](./pdf/39.pdf)
- [Kubernetes. GDocs](https://docs.google.com/document/d/16K30QIvIm_4Fc-uQmENKeUerSNBZYfY2LDwDKfiWarI) - [PDF](./pdf/40.pdf)
- [Kubernetes. Часть 2. GDocs](https://docs.google.com/document/d/1fBk98Y5lIf4zQtgWYQxbJNu_WzaYFyapHhKdbOyzcAE) - [PDF](./pdf/41.pdf)

*Автоматизация администрирования инфраструктуры*
- [Ansible. GDocs](https://docs.google.com/document/d/19h64evXK9vIcPKQm24jo95gMBDl8nGD_Nk2vyxCdqQQ) - [PDF](./pdf/42.pdf)
- [Ansible. Часть 2. GDocs](https://docs.google.com/document/d/1-l3bpVy7GGDlByeqVhWUFq-cz4EPObnNwsMePUVarvE) - [PDF](./pdf/43.pdf)
- [Terraform. GDocs](https://docs.google.com/document/d/1bevVUZ76tb0BcqR2z2XDoPg9g-d_lKyyhLSCP7_DvhY) - [PDF](./pdf/44.pdf)
- [Подъем инфраструктуры в облаке. GDocs](https://docs.google.com/document/d/1QZdiSDrE-wAnk-1lcIQm_S2zNB6S_wkcZ10frqnsMis) - [PDF](./pdf/45.pdf)

*Введение в DevOps*
- [Git. GDocs](https://docs.google.com/document/d/1QmkzZqS4ghlVR2H9uBnxIYEwp22wfh6lK6efd8h_yRI) - [PDF](./pdf/46.pdf)
- [Что такое DevOps. CI/CD. GDocs](https://docs.google.com/document/d/1kiJZVvQSnEIZvOVKmZ6VEfFXrcsQweNxieRFOsheWBM) - [PDF](./pdf/47.pdf)
- [GitLab. GDocs](https://docs.google.com/document/d/16-PsN4zZW13ngC1vs-gJlh1ayWUPEm-H8zaSMhVigrM) - [PDF](./pdf/48.pdf)

## Мониторинг и отказоустойчивость
*Мониторинг*
- ["Системы мониторинга. Облачный мониторинг Яндекс.Облако".](https://gitlab.com/netology-alex31bel/srlb-homework/-/blob/main/9-01.md)
- [Zabbix](https://gitlab.com/netology-alex31bel/srlb-homework/-/blob/main/9-02.md)
- [Zabbix. Часть 2](https://gitlab.com/netology-alex31bel/srlb-homework/-/blob/main/9-03.md)
- [Prometheus](https://gitlab.com/netology-alex31bel/srlb-homework/-/blob/main/9-04.md)
- [Prometheus. Часть 2](https://gitlab.com/netology-alex31bel/srlb-homework/-/blob/main/9-05.md)

*Отказоустойчивость*
- [Keepalived/vrrp](https://gitlab.com/netology-alex31bel/srlb-homework/-/blob/main/10-01.md)
- [Кластеризация](https://docs.google.com/document/d/1H7pqlW4bcKk8sPh5dMvqKtBLh0dnE5rtTxnqGm0qqI4/edit)
- [Pacemaker](https://gitlab.com/netology-alex31bel/srlb-homework/-/blob/main/10-03.md)
- [Резервное копирование. Bacula](https://gitlab.com/netology-alex31bel/srlb-homework/-/blob/main/10-04.md)
- [Балансировка нагрузки. HAProxy/Nginx](https://gitlab.com/netology-alex31bel/srlb-homework/-/blob/main/10-05.md)
- [Disaster recovery](https://gitlab.com/netology-alex31bel/srlb-homework/-/blob/main/10-06.md)
- [Отказоустойчивость в облаке](https://gitlab.com/netology-alex31bel/srlb-homework/-/blob/main/10-07.md)

## Базы данных и информационная безопасность
*Системы хранения и передачи данных*
- [Базы данных, их типы](https://gitlab.com/netology-alex31bel/sdb-homeworks/-/blob/main/11-01.md)
- [Кеширование Redis/memcached](https://gitlab.com/netology-alex31bel/sdb-homeworks/-/blob/main/11-02.md)
- [ELK](https://gitlab.com/netology-alex31bel/sdb-homeworks/-/blob/main/11-03.md)
- [Очереди RabbitMQ](https://gitlab.com/netology-alex31bel/sdb-homeworks/-/blob/main/11-04.md)

*Реляционные базы данных*
- [Базы данных](https://gitlab.com/netology-alex31bel/sdb-homeworks/-/blob/main/12-01.md)
- [Работа с данными (DDL/DML)](https://gitlab.com/netology-alex31bel/sdb-homeworks/-/blob/main/12-02.md)
- [SQL. Часть 1](https://gitlab.com/netology-alex31bel/sdb-homeworks/-/blob/main/12-03.md)
- [SQL. Часть 2](https://gitlab.com/netology-alex31bel/sdb-homeworks/-/blob/main/12-04.md)
- [Индексы](https://gitlab.com/netology-alex31bel/sdb-homeworks/-/blob/main/12-05.md)
- [Репликация и масштабирование.](https://gitlab.com/netology-alex31bel/sdb-homeworks/-/blob/main/12-06.md)
- [Резервное копирование.](https://gitlab.com/netology-alex31bel/sdb-homeworks/-/blob/main/12-08.md)
- [Базы данных в облаке](https://gitlab.com/netology-alex31bel/sdb-homeworks/-/blob/main/12-09.md)

*Информационная безопасность*
- [Уязвимости и атаки на информационные системы](https://gitlab.com/netology-alex31bel/sdb-homeworks/-/blob/main/13-01.md)
- [Защита хоста](https://gitlab.com/netology-alex31bel/sdb-homeworks/-/blob/main/13-02.md)

## Итоговый модуль профессии Системный администратор
- [Дипломная работа профессии Системный администратор](https://gitlab.com/netology-alex31bel/sys-diplom/)


[![Сertificate](./img/certificate.jpg)](https://netology.ru/sharing/59a7c1820949ec7f6540e05fdd62b8bc)